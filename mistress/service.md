# 定期保養表

## 簡易保養表

品項|時間 或 里程
--|--
機油|每 0.5 年 或 每 1 萬公里
機油芯|每 0.5 年 或 每 1 萬公里
放油塞墊片|每 0.5 年 或 每 1 萬公里
正時皮帶|每 10 年 或 每 20 萬公里
綜合皮帶|每 8 年 或 每 16 萬公里
空氣濾芯|每 2 年 或 每 4 萬公里
冷氣濾網|每 2 年 或 每 4 萬公里
汽油濾芯|每 2 年 或 每 4 萬公里
火星塞|每 3 年 或 每 6 萬公里
變速箱油|每 2 年 或 每 4 萬公里
冷卻液|每 3 年 或 每 6 萬公里<br>(8 年 或 16 萬 後)
煞車油|每 2 年 或 每 4 萬公里

## 保養總表

R = 更換  
I = 檢查，若不良則更換

時間/里程<br>(月/萬公里)|1/0.1|6/1|12/2|18/3|24/4|30/5|36/6|42/7|48/8|54/9|60/10
--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
機油|I|R|R|R|R|R|R|R|R|R|R
機油芯|I|R|R|R|R|R|R|R|R|R|R
放油塞墊片|I|R|R|R|R|R|R|R|R|R|R
正時皮帶|-|-|-|-|-|-|-|-|-|-|-
綜合皮帶|I|I|I|I|I|I|I|I|I|I|I
空氣濾芯|I|I|I|I|R|I|I|I|R|I|I
冷氣濾網|I|I|I|I|R|I|I|I|R|I|I
汽油濾芯|-|-|-|-|R|-|-|-|R|-|-
火星塞|-|-|-|-|-|I|R|I|I|I|I
變速箱油|-|-|-|-|R|-|-|-|R|-|-
冷卻液|I|I|I|I|I|I|I|I|I|I|I
煞車油|I|I|I|I|R|I|I|I|R|I|I

![保養表](images/service-table.jpg)

## 英國 Ford 建議保養時間

每年 或 每 12.5k miles (約 2 萬公里)
