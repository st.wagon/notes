# 車主手冊

## 中文版

[官網載點](https://www.ford.com.tw/content/dam/Ford/website-assets/ap/tw/owner/owners-manual/Focus_ST_Owner_Manual.pdf)  
[備份](Focus_ST_Owner_Manual.pdf)

## 英文版 (所有 Focus 共用)

[官網載點](https://www.fordservicecontent.com/Ford_Content/Catalog/owner_information/CG3784en-202008-20200908080223.pdf)  
[備份](Focus_Owner_Manual-English.pdf)
