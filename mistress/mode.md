# 駕駛模式

-|正常|運動|賽道|防滑
--|:--:|:--:|:--:|:--:
懸吊 CCD|Normal|Sport|Track|Normal
電子式限滑差速器 eLSD|Normal|Sport|Track|Slippery
轉向 EPAS|Normal|Sport|Track|Normal
引擎 ECM|Normal|Sport|Sport|Slippery
引擎聲浪 ESE|Normal|Sport|Track|Normal
穩定系統 ESC|Normal|Normal|Reduced|Slippery
循跡系統 TC|Normal|Normal|**OFF**|Normal
電子煞車增壓輔助 EBB|Normal|Normal|Track|Normal
空調系統 HVAC|Normal|ECO|ECO|Normal
怠速熄火|ON|**OFF**|**OFF**|ON
防撞輔助|ON|ON|**OFF**|ON
車道維持|ON|ON|**OFF**|ON
最高檔位|7|6|6|7

註：ST 沒有 ECO Mode :P
