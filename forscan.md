# FORScan

## 出廠電腦預設值下載 (六和調整前)

[Motocraft](https://www.motorcraftservice.com/AsBuilt/)

## 已測試之隱藏功能

功能|說明|設定路徑
--|--|--
自動上鎖|行駛後車門自動上鎖|車輛→
關閉模擬聲浪|-|無
關閉安全帶警告聲|-|無
修改 SYNC 3 Bluetooth 名稱|預設為"Ford Focus"，可改成"Ford Focus ST"|無
關閉怠速熄火|關閉電瓶管理系統可讓怠速熄火失效<br>**注意**：可能減少電瓶壽命，不建議使用|無
