# 掛牌數

## 2020

-|八月|九月|十月|十一月|十二月
--|--|--|--|--|--
數量|72|31|2|1|147
累積數量|-|103|105|106|253

## 2021

-|一月|二月|三月|四月|五月|六月|七月|八月|九月|十月|十一月|十二月
--|--|--|--|--|--|--|--|--|--|--|--|--
數量|256|81|78|113|90|65|23|4|1|2|0|0
累積數量|509|590|668|781|871|936|959|963|964|966|966|966

## 2022

-|一月|二月|三月|四月|五月|六月|七月
--|--|--|--|--|--|--|--
數量|44|6|3|1|0|0|0
累積數量|1010|1016|1019|1020|1020|1020|1020

Reference:  
[銷售數據 - 7Car](https://www.7car.tw/article/second/18)  
[2022 年 07 月](https://www.7car.tw/articles/read/83599)  
[2022 年 06 月](https://www.7car.tw/articles/read/82760)  
[2022 年 05 月](https://www.7car.tw/articles/read/81931)  
[2022 年 04 月](https://www.7car.tw/articles/read/81188)  
[2022 年 03 月](https://www.7car.tw/articles/read/80553)  
[2022 年 02 月](https://www.7car.tw/articles/read/79850)  
[2022 年 01 月](https://www.7car.tw/articles/read/79274)  
[2021 年 12 月](https://www.7car.tw/articles/read/78656)  
[2021 年 11 月](https://www.7car.tw/articles/read/78054)  
[2021 年 10 月](https://www.7car.tw/articles/read/77544)  
[2021 年 09 月](https://www.7car.tw/articles/read/76964)  
[2021 年 08 月](https://www.7car.tw/articles/read/76410)  
[2021 年 07 月](https://www.7car.tw/articles/read/75839)  
[2021 年 06 月](https://www.7car.tw/articles/read/75303)  
[2021 年 05 月](https://www.7car.tw/articles/read/74747)  
[2021 年 04 月](https://www.7car.tw/articles/read/74079)  
[2021 年 03 月](https://www.7car.tw/articles/read/73476)  
[2021 年 02 月](https://www.7car.tw/articles/read/72860)  
[2021 年 01 月](https://www.7car.tw/articles/read/72227) ([7car 應該是誤植](https://news.u-car.com.tw/news/article/64924))  
[2020 年 12 月](https://www.7car.tw/articles/read/71623)  
[2020 年 11 月](https://www.7car.tw/articles/read/70876)  
[2020 年 10 月](https://www.7car.tw/articles/read/70265)  
[2020 年 09 月](https://www.7car.tw/articles/read/69696)  
[2020 年 08 月](https://www.7car.tw/articles/read/68918)
