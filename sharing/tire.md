# 輪胎

## 輪框寬度 (J值) 與胎寬對應表

J值|最小胎寬<br>操控至上<br>安裝極限|合理胎寬<br>操控取向<br>跑車建議|合理胎寬<br>舒適取向<br>房車建議|最大胎寬<br>舒適至上<br>安裝極限
:--:|:--:|:--:|:--:|:--:
5   |155|165|175|185
5.5 |165|175|185|195
6   |175|185|195|205
6.5 |185|195|205|215
7   |195|205|215|225
7.5 |205|215|225|235
8   |215|225|235|245
8.5 |225|235|245|255
9   |235|245|255|265
9.5 |245|255|265|275
10  |255|265|275|285
10.5|265|275|285|295
11  |275|285|295|305
11.5|285|295|305|315
12  |295|305|315|325
12.5|305|315|325|335

## 原廠框可用輪胎清單

更新時間： 2021-05-12

廠牌|花紋|耐磨指數|尺寸|價格|註
--|--|:--:|--|--|--
普利司通 Bridgestone|[RE004](https://www.bridgestone.com.tw/zh/tire/potenza-re004)|220|235/35/19|6600|台灣
普利司通 Bridgestone|[S001](https://www.bridgestone.com.tw/zh/tire/potenza-s001)|280|235/35/19|7400|日本
普利司通 Bridgestone|[S007A](https://www.bridgestone.com.tw/zh/tire/potenza-s007a)|220|235/35/19|7400|日本
馬牌 Continental|[EXC Sport](https://www.continental.tw/car/tires/extremecontact-sport)|340|235/35/19|9300|
馬牌 Continental|[MC6](https://www.continental.tw/car/tires/contimaxcontact-mc6)|340|235/35/19|8000|
馬牌 Continental|[SC6](https://www.continental.tw/car/tires/sportcontact-6)|240|235/35/19|9300|
登祿普 Dunlop|[SP SPORT MAXX 050+](https://www.dunlop-kc.com.tw/products.aspx?cid=2015070020&pid=2016050004)|240|235/35/19|6600|日本
飛隼 Falken|[FK510](https://www.falkentyre.com.tw/product/detail/261)|300|235/35/19|6300|
固特異 Goodyear|[F1 ASYMMETRIC 5 (F1A5)](https://www.goodyear.com.tw/tyres/goodyear-eagle-f1-asymmetric-5)|300|235/35/19|7600|德國
固特異 Goodyear|[F1 SuperSport](https://www.goodyear.com.tw/tyres/goodyear-eagle-f1-supersport)|?|235/35/19|7800|德國
固特異 Goodyear|[F1 SuperSport R](https://www.goodyear.com.tw/tyres/goodyear-eagle-f1-supersport-r)|?|235/35/19|10200|德國
韓泰 Hankook|Ventus S1 evo2 (K117)|?|235/35/19|5600|韓國
韓泰 Hankook|Ventus S1 evo3 (K127)|340|235/35/19|6100|韓國
錦湖 KUMHO|[Ecsta PS71](http://www.kumhotw.com.tw/product-PS71-PS71.html)|320|235/35/19|5200|韓國
錦湖 KUMHO|[Ecsta PS91](http://www.kumhotw.com.tw/product-PS91-PS91.html)|260|235/35/19|5700|韓國
錦湖 KUMHO|[Ecsta V720](http://www.kumhotw.com.tw/product-V720-V720.html)|?|235/35/19|5900|韓國
瑪吉斯 Maxxis|VS01|?|235/35/19|4600|台灣
瑪吉斯 Maxxis|VS5|320|235/35/19|6700|台灣
米其林 Michelin|[Primacy 4](https://www.michelin.com.tw/products.php?id=1143)|340|235/40/19|8900|
米其林 Michelin|[Pilot Sport 4 S (PS4S)](https://www.michelin.com.tw/products.php?id=1138)|300|235/35/19|8300|
米其林 Michelin|[Pilot Sport CPU 2](https://www.michelin.com.tw/products.php?id=1137)|?|235/35/19|10200|
南港 NANKANG|[AS-2+](http://www.nankang-tyre.com/index.php?active=ProductDetail&item=2)|?|235/35/19|4500|中國
南港 NANKANG|[NS-2](http://www.nankang-tyre.com/index.php?active=ProductDetail&item=22)|200|235/35/19|5300|台灣 單導
南港 NANKANG|[NS-20](http://www.nankang-tyre.com/index.php?active=ProductDetail&item=21)|360|235/35/19|5400|台灣 單導
南港 NANKANG|[NS-25](http://www.nankang-tyre.com/index.php?active=ProductDetail&item=23)|500|235/35/19|4500|台灣
日東 Nitto|[INVO](http://www.nittotire.com.tw/product-%e8%b1%aa%e8%8f%af%e9%ab%98%e6%80%a7%e8%83%bd%e9%81%8b%e5%8b%95%e5%9e%8b%e8%bc%aa%e8%83%8e-INVO.html)|260|235/35/19|7000|日本
日東 Nitto|[NT830+](http://www.nittotire.com.tw/product-NT-830-plus-NT-830-plus.html)|300|235/35/19|6000|日本
倍耐力 Pirelli|[P-ZERO](https://www.pirelli.com/tyres/zh-tw/car/find-your-tyres/tyre-catalogue-car)|220|235/35/19|7000|
倍耐力 Pirelli|[P ZERO ROSSO](https://www.pirelli.com/tyres/zh-tw/car/find-your-tyres/tyre-catalogue-car)|220|235/35/19|7800|
東洋 Toyo| [Proxes Sport (PXSP)](https://toyotires.tw/product/pxsp/)|240|235/35/19|6700|日本
東洋 Toyo| [TR1](https://toyotires.tw/product/pxtr1/)|280|235/35/19|6100|日本
橫濱 Yokohama| [AE51](https://www.yokohamatire.com.tw/product/98)|300|235/35/19|8300|日本

## 三大品牌輪胎等級定位 (依乾地性能)

![tire-tier](images/tire_tier.jpg)

> 出處：[Hank汽車部落格](https://www.facebook.com/photo?fbid=314697560013328)
